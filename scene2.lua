----------- scene2 ----------------
local composer = require("composer")
local widget = require("widget")
local content = require("content")
local dFile = require("dFile")
local scene = composer.newScene()
-- переменные экрана
local baseDir = system.DocumentsDirectory 
local screenW, screenH = display.contentWidth, display.contentHeight
local viewableScreenW, viewableScreenH = display.viewableContentWidth, display.viewableContentHeight
--------------------------------------------------------------------------------------------------------------------------------
--[[	-----------------------------------------РАСШИРЕНИЕ---------------------------------------------------------------------
local function showImage(self, event)
	if (event.phase == "ended") then
		composer.gotoScene("showImage", {time=250, effect="crossFade", params={start=event.target.index, images}})
	end
	return true
end
--]]
--------------------------------------------------------------------------------------------------------------------------------
function scene:create( event )
	local sceneGroup = self.view
	--local images = {}---------------------------------------------------- Расширение. Массив для расширения
	local top = 100
	local space = 150 

	background2 = display.newImageRect("background2.jpg", _W, _H)
	
	background2.x = display.contentCenterX - 80
	background2.y = display.contentCenterY
	sceneGroup:insert( background2 )

	local function scrollListener( event )
		local phase = event.phase
		if (event.limitReached) then
			if (event.direction == "up") then print( "Reached bottom limit" )
			elseif (event.direction == "down") then print( "Reached top limit" )
			--elseif (event.direction == "left") then print( "Reached right limit" )
			--elseif (event.direction == "right") then print( "Reached left limit" )
			end
		end
		return true
	end

	local scrollView = widget.newScrollView
	{
		left = 0,
		top = 0,
		width = display.contentWidth,
		height = display.contentHeight,
		scrollWidth = display.contentWidth,
		scrollHeight = display.contentHeight,
		horizontalScrollDisabled = true,
		verticalScrollDisabled = false,
		listener = scrollListener,
		hideBackground = true
	}
	
	-- выводим каждую картинку на сцену
	-- подгружаем каждую картинку на экран и позиционируем
	for i=1,#parserImage do
		local thumb = display.newImage(parserImage[i].thumb, baseDir)
		local h = viewableScreenH-top
		
		if thumb.width > viewableScreenW or thumb.height > h then
			if thumb.width/viewableScreenW > thumb.height/h then 
				thumb.xScale = viewableScreenW/thumb.width
				thumb.yScale = viewableScreenW/thumb.width
			else
				thumb.xScale = h/thumb.height
				thumb.yScale = h/thumb.height
			end		 
		end
		-- позиционируем каждую картинку на экране
		if (i>1) then 
			thumb.y = thumb.height/2 + space + s
			s = thumb.y  + space
		else
			thumb.y = top
			s = top
		end
		thumb.x = display.contentCenterX
		--thumb.touch = showImage--------------------------- Расширение.
		--images[i] = thumb -------------------------------- Расширение. Для вывода на след. сцену увеличенной картинки 
		--thumb:addEventListener("touch", showImage)-------- Расширение. 
		scrollView:insert( thumb )
		scrollView.x = display.contentCenterX - 40
		sceneGroup:insert( scrollView )
	end
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase

	if (phase == "did") then
		--thumb:addEventListener( "touch", thumb )	-------- Расширение.
		composer.removeScene( "scene1" )
	end
end

function scene:hide( event )
	local sceneGroup = self.view
end

function scene:destroy( event )
	local sceneGroup = self.view
end

-- listener's
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene

