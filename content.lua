
-- Модуль чтения (декодирования) *.json файлов
----------------------------------------------
module(..., package.seeall)

local json = require ("json")
local content = {}

local function loadFile(filename)
	local path = system.pathForFile(filename, system.ResourceDirectory);
	local contents = "";
	local myTable = {};
	local file = io.open(path, "r");
	if (file) then
		local contents = file:read( "*a" );
		myTable = json.decode( contents );
		io.close( file );
		return myTable;
	end
	return nil
end

content = loadFile("images.json");
if not content then 
	print("Decoded failed");
else 
	print ("Decoded succsessful")
end

return content