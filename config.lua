--
-- For more information on config.lua see the Corona SDK Project Configuration Guide at:
-- https://docs.coronalabs.com/guide/basics/configSettings
--

application =
{
	content =
	{
		width = 640, 		-- 768
		height = 960, 		-- 1024
		scale = "zoomEven",	--"zoomEven"
		fps = 30,
		xAlign = "left",	-- лишняя херня  
		yAlign = "bottom",	--	тоже лишняя
		
		--[[
		imageSuffix =
		{
			    ["@2x"] = 2,
			    ["@4x"] = 4,
		},
		--]]
	},
}
