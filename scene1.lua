---------------------- scene1--------------
local composer = require("composer")
local scene = composer.newScene()

local myText

local function showScene2() 
	local options = {
		effect = "fade",
		time = 700
	}
	composer.gotoScene( "scene2", options  )
end

function scene:create( event )
	local sceneGroup = self.view
	
	background = display.newImageRect( "background.jpg", _W, _H)
	background.x = display.contentCenterX - 80
	background.y = display.contentCenterY
	sceneGroup:insert(background)
	
	myText = display.newText("Gallery", 100,150, native.sysstemFont, 42)
	sceneGroup:insert(myText)

end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase

	if (phase == "did") then
		timer.performWithDelay( 3000, showScene2) ------------ Переход на следующую сцену
	end
end

function scene:hide(event)
	local sceneGroup = self.view
end

function scene:destroy(event)
	local sceneGroup = self.view
end

-- listener's
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene