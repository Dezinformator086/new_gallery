-- Ну очень простая галерея
-- Глобальные переменные
_W = display.contentWidth;
_H = display.contentHeight;
parserImage = {}

local content = require("content")
local dFile = require("dFile")
local composer = require("composer")

-- Прячем строку состояния
display.setStatusBar(display.HiddenStatusBar)
composer.gotoScene( "scene1", "fade", 400)
---------Загрузка картинок
for index, value in next, content.images do
	dFile.downloadImage(index, index..".png")
end