
-- Модуль загрузки картинок по URL'ам и формирования массива с адресами картинок на диске 
------------------------------
module(..., package.seeall )

local content = require( "content")

function dFile.downloadImage(index, filename)

	local baseDirectory = system.DocumentsDirectory
    local filename = tostring( filename )

	local function networkListener( event )
    	if ( event.isError ) then
        	print( "Network error - download failed: ", event.response )
    	elseif ( event.phase == "began" ) then
        	print( "Progress Phase: began" )
    	elseif ( event.phase == "ended" ) then
    	end
	end
	-- загружаем картинку
    network.download(content.images[index], "GET", networkListener, filename, baseDirectory)
    -- забрать адрес каждой картинки, запихать его в массив
    path = system.pathForFile( filename, system.DocumentsDirectory )
    path_toString = tostring( path )
    parserImage[index] = {thumb = ""..filename..""}
    --print( parserImage[index].thumb ) ------------------------------ для теста построчного вывода массива с адресами к картинкам
end

return dFile